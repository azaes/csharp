﻿using System.Collections.Generic;
using MyTwitter.Models;

namespace MyTwitter.ViewModels
{
    public class ChatViewModel
    {
        public Chat Chat { get; set; }
        public List<ApplicationUser> Users { get; set; }
        public string CurrentUserId { get; set; }
        public List<ChatMessage> Messages { get; set; }
    }
}