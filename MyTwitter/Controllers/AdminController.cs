﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MyTwitter.Data;
using MyTwitter.Models;

namespace MyTwitter.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _context;
        private readonly RoleManager<TwitterRole> _roleManager;

        public AdminController(
            UserManager<ApplicationUser> userManager,
            ApplicationDbContext context,
            RoleManager<TwitterRole> roleManager)
        {
            _userManager = userManager;
            _context = context;
            _roleManager = roleManager;
        }

        public async Task<IActionResult> Index()
        {
            List<ApplicationUser> users = _userManager.Users.ToList();

            foreach (ApplicationUser applicationUser in users)
            {
                IList<string> roles = await _userManager.GetRolesAsync(applicationUser);
                applicationUser.Roles = roles.ToList();
            }

            return View(users);
        }

        public virtual async Task<IdentityResult> LockUserAccount(ApplicationUser user)
        {
            IdentityResult result = await _userManager.SetLockoutEnabledAsync(user, true);
            if (result.Succeeded)
            {
                result = await _userManager.SetLockoutEndDateAsync(user, DateTimeOffset.MaxValue);
                user.IsBlocked = true;
                _context.SaveChanges();
            }
            return result;
        }
        public virtual async Task<IdentityResult> UnlockUserAccount(ApplicationUser user)
        {
            var result = await _userManager.SetLockoutEnabledAsync(user, false);
            if (result.Succeeded)
            {
                await _userManager.ResetAccessFailedCountAsync(user);
                user.IsBlocked = false;
                _context.SaveChanges();
            }
            return result;
        }

        [HttpPost]
        public async Task<IActionResult> Block(string id)
        {
            ApplicationUser user = await _userManager.FindByIdAsync(id);
            await LockUserAccount(user);
            return RedirectToAction("Index");
        }
        [HttpPost]
        public async Task<IActionResult> UnBlock(string id)
        {
            ApplicationUser user = await _userManager.FindByIdAsync(id);
            await UnlockUserAccount(user);
            return RedirectToAction("Index");
        }

        public IActionResult Roles()
        {
            List<TwitterRole> roles = _roleManager.Roles.ToList();

            return View(roles);
        }

        [HttpPost]
        public async Task<IActionResult> CreateRole(string roleName)
        {
            TwitterRole role = new TwitterRole()
            {
                Name = roleName
            };
            await _roleManager.CreateAsync(role);

            return RedirectToAction("Roles");
        }

        public async Task<IActionResult> Profile(string id)
        {
            ApplicationUser user = await _userManager.FindByIdAsync(id);
            IList<string> roles = await _userManager.GetRolesAsync(user);
            user.Roles = roles.ToList();
            ViewBag.Roles = _roleManager.Roles.ToList();

            return View(user);
        }

        public async Task<IActionResult> SetRoles(IEnumerable<string> role, string id)
        {
            if(!role.Any()) return RedirectToAction("Profile", new { id });
            
            ApplicationUser user = await _userManager.FindByIdAsync(id);
            IList<string> roles = await _userManager.GetRolesAsync(user);
            foreach (string r in roles)
            {
                await _userManager.RemoveFromRoleAsync(user, r);
            }
            //await _userManager.AddToRolesAsync(user, role);
            foreach (string r in role)
            {
                await _userManager.AddToRoleAsync(user, r);
            }
            return RedirectToAction("Profile", new {id});
        }
    }
}