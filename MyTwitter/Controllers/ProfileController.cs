﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyTwitter.Data;
using MyTwitter.Models;
using MyTwitter.ViewModels;
using Newtonsoft.Json;

namespace MyTwitter.Controllers
{
    public class ProfileController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public ProfileController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index(string id)
        {
            ApplicationUser user = await _userManager.FindByIdAsync(id);
            List<Post> posts = await _context.Posts.Where(p => p.AuthorId == id).ToListAsync();

            ProfileViewModel model = new ProfileViewModel()
            {
                User = user,
                Posts = posts
            };

            return View(model);
        }

        public IActionResult Search(string key)
        {
            List<ApplicationUser> users = _context.ApplicationUser.ToList();
            if (!string.IsNullOrWhiteSpace(key))
            {
                users = users.Where(u => u.UserName.Contains(key)).ToList();

            }

            return View(users);
        }

        [HttpPost]
        public string SearchForAdding(string key)
        {
            List<ApplicationUser> users = _context.ApplicationUser.ToList();
            if (!string.IsNullOrWhiteSpace(key))
            {
                users = users.Where(u => u.UserName.Contains(key)).ToList();

            }

            return JsonConvert.SerializeObject(users.Select(u => u.UserName), 
                new JsonSerializerSettings() {ReferenceLoopHandling = ReferenceLoopHandling.Ignore});
        }
    }
}