﻿using System;

namespace MyTwitter.Models
{
    public class ChatMessage
    {
        public int Id { get; set; }

        public string Content { get; set; }

        public DateTime MessageDate { get; set; }

        public int ChatId { get; set; }
        public Chat Chat { get; set; }

        public string AuthorId { get; set; }
        public ApplicationUser Author { get; set; }
    }
}