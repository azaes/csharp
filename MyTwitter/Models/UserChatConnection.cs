﻿namespace MyTwitter.Models
{
    public class UserChatConnection
    {
        public int Id { get; set; }

        public int ChatId { get; set; }
        public Chat Chat { get; set; }

        public string UserId { get; set; }
        public ApplicationUser User { get; set; }
    }
}